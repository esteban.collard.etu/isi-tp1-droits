# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Collard, Esteban, esteban.collard.etu@univ-lille.fr

- Nom, Prénom, email: Finard, Alexis, alexis.finard.etu@univ-lille.fr 

## Question 1

Commande:   
	- `sudo adduser toto`   
	- `sudo addgroup toto ubuntu`   

Réponse:   
	Le processus peut écrire car le fichier titi.txt est accécible en écriture uniquement pour les utilisateurs du groupe ubuntu.
	Etant donner que l'utilisateur toto fait partie du groupe ubuntu, le processus lancé par toto peut accéder au fichier titi.txt en écriture

## Question 2

Réponse:   
	Le caractère x pour un répertoire correspond à la possibilité d'y accéder.

Commande sur l'utilisateur ubuntu:   
	- `mkdir mydir`   
	- `chmod 767 mydir/`   

Commande sur l'utilisateur toto:   
	- `cd mydir/`   

Réponse:   
	Une fois le dossier mydir créer par ubuntu et le droit d'éxécution supprimer les utilisateurs du groupe ubuntu, la commande ouvrir le dossier mydir de l'utilisateur toto à retourner "Permission denied".   
	Cela est du au fait que toto appartient au groupe ubuntu. Il ne peut donc ouvrir le dossier.

Commande sur l'utilisateur ubuntu:   
	- `touch mydir/data.txt`

Commande sur l'utilisateur toto:   
	- `ls -al mydir/`

Réponse:   
Une fois le fichier créer par l'utilisateur ubuntu, la commande `ls -al mydir/` à retourner :

    ls: cannot access 'mydir/.': Permission denied
    ls: cannot access 'mydir/..': Permission denied
    ls: cannot access 'mydir/data.txt': Permission denied
    total 0
    d????????? ? ? ? ?            ? .
    d????????? ? ? ? ?            ? ..
    -????????? ? ? ? ?            ? data.txt
    
Nous arrivons donc à connaitre l'existence du fichier data.txt, cependant, il est impossible de connaitre la taille du fichier ainsi que les permissions accordées.
Nous avons ce résultat car l'utilisateur n'a pas les droits d'accés au dossier mydir.
	

## Question 3

Réponse:   
	Les différents ids ont la valeur 1001. Le processus n'arrive pas à ouvrir le fichier mydir/mydata.txt en lecture.

Commande:   
	- `chmod u+s mydir/`

Réponse:   
	Les ids ont toujours la valeur 1001 sauf EUID qui à la valeur 1000. Le processus n'arrive toujours pas à ouvrir le fichier mydir/mydata.txt en lecture.
	

## Question 4

Commande:   
	- `chmod u+s suid.py`

Réponse:   
les ids ont las valeurs :

    EUID= 1001   
    EGID= 1001   
    RUID= 1001   
    hisRGID= 1001   

## Question 5

Commande:   
	- `sudo nano /etc/passwd`

Réponse:   
	La commande `chfn` sert à modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd".

Commande:   
	`ls -al /usr/bin/chfn`
		-rwsr-xr-x 1 qroot root 85064 May 28  2020 /usr/bin/chfn

Réponse:   
	La commande `chfn` à comme permission :   
		- un accés lecture, ecriture, exécution pour le propriétaire qroot   
		- un accés lecture, exécution pour le groupe root et les autres utilisateurs   

Commande:   
	- `chfn`

	Password:
	Changing the user information for toto
	Enter the new value, or press ENTER for the default
	Full Name:
	Room Number []: 23
	Work Phone []: 0303030303
	Home Phone []: 0606060606

Réponse:   
	On voit bien que les informations ont été mis à jour.

## Question 6

Réponse:   
	Les mots de passe des utilisateurs sont stocké dans le fichier /etc/shadow car il ne sont accessible uniquement par qroot ou par les utilisateurs de groupe shadow

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








