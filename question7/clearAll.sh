#!/bin/sh

## Supprimer dossiers*
sudo rm -rf dir_a
sudo rm -rf dir_b
sudo rm -rf dir_c

## Supprimer utilisateurs
sudo deluser admin
sudo deluser lambda_a
sudo deluser lambda_b

## Supprimer groupes
sudo delgroup groupe_a
sudo delgroup groupe_b