#!/bin/bash

## creation utilisateurs
sudo adduser lambda_a
sudo adduser lambda_b

## creation groupes
groupadd groupe_a
groupadd groupe_b

usermod -a -G groupe_a lambda_a
usermod -a -G groupe_b lambda_b