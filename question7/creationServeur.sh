#!/bin/sh

if id "admin" >/dev/null 2>&1
then
sudo adduser admin
admin
admin
fi


mkdir dir_a
sudo addgroup groupe_a

if id "lambda_a" >/dev/null 2>&1
then
sudo adduser lambda_a
lambda_a
lambda_a
fi


sudo usermod -g groupe_a lambda_a
sudo chown admin:groupe_a dir_a
sudo chmod u+rwx,g+rw,+t dir_a


mkdir dir_b
sudo addgroup groupe_b

if id "lambda_b" >/dev/null 2>&1
then
sudo adduser lambda_b
lambda_b
lambda_b
fi

sudo usermod -g groupe_b lambda_b
sudo chown admin:groupe_b dir_b
sudo chmod u+rwx,g+rw,+t dir_b

mkdir dir_c
sudo chown admin: dir_c
sudo chmod u+rwx,o+r dir_c