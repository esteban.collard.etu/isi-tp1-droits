#!/bin/bash

## ajout admin au groupes
sudo usermod -a -G groupe_a admin
sudo usermod -a -G groupe_b admin

su admin

## Creation du dossier dir_a
mkdir dir_a
chown admin:groupe_a dir_a
chmod +t dir_a
chmod o -r dir_a
chmod g +s dir_a

## Creation du dossier dir_b
mkdir dir_b
chown admin:groupe_b dir_b
chmod +t dir_b
chmod o -r dir_b
chmod g +s dir_b

## Creation du dossier dir_c
mkdir dir_c
chmod g +s dir_c