#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	//affichage id
	printf("EUID=%ld\n", (long) geteuid());
	printf("EGID=%ld\n", (long) getegid());
	printf("RUID=%ld\n", (long) getuid());
	printf("RGID=%ld\n", (long) getgid());

	//contenue mydata.txt
	//system("cat mydir/mydata.txt");

	FILE* fichier;
	fichier = fopen("mydir/mydata.txt", "r");

	if (fichier != NULL)
	{
		char ch;
		// On peut lire le fichier
		while((ch=fgetc(fichier))!=EOF){
			printf("%c", ch);
		}

		// On ferme le fichier mydir/mydata.txt
		fclose(fichier);
	}
	else
	{
		// Impossible d'ouvrir le fichier 
		printf("Impossible d'ouvrir le fichier mydir/mydata.txt\n");
	}

	return 0;
}
