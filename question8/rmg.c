#include <stdlib.h>
#include <stdio.h>

#include "check_pass.h"

int main(int argc, char *argv[]) {
    if(argc<3){
        printf("parametre non valide\n");
        printf("exemple utilisation\n");
        printf("rmg file password\n");
    }
    else{
        uid_t uid = getuid();
        gid_t gid = getuid();

        // Verification id
        if(getIdentifiant(argv[1])!=uid){
            printf("id non valide\n");
            exit(EXIT_FAILURE);
        }

        //verification mdp
        if(verification_motDePasse(gid,argv[2])){
            if(remove(argv[1]) == 0){
                printf("Fichier supprimer\n");
            }

            else{ printf("Impossible de supprimer le fichier\n"); }        
        }

        else{ printf("Mot de passe non valide\n"); } 
    }

}