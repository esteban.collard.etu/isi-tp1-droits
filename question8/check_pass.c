#include "check_pass.h"

int verification_motDePasse(int identifiant, char *pwd){
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    char *idRead = NULL;
    char *pwdRead = NULL;

    fp = fopen("/home/admin/passwd", "r");
    if (fp == NULL){
        printf("Erreur fichier mot de passe\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        idRead = strtok(line, ":");
        if(atoi(idRead) == identifiant){
            pwdRead = strtok(NULL,":");
            pwdRead = strtok(pwdRead,"\n");
            fclose(fp);
            return strcmp(pwdRead,pwd);
        }
    }
}

int getIdentifiant(char *file) {

    struct stat stats;

    if (stat(file, &stats) == 0) {
        return stats.st_gid;
    } else {
        perror("impossible de recuperer les identifiants du fichier");
        exit(EXIT_FAILURE);
    }
}