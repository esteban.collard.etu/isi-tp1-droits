# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Collard, Esteban, esteban.collard.etu@univ-lille.fr

- Nom, Prénom, email: Finard, Alexis, alexis.finard.etu@univ-lille.fr 

## Question 1

Réponse:
Le processus devrait pouvoir écrire tant que son EUID est égal à son RUID

## Question 2 

Réponse:
-L'execution d'un repertoire correspond à l'accès à celui-ci. On en a besoin pour faire cd (chdir).
-Il est donc impossible d'acceder à mydir à partir de l'utilisateur toto car son groupe principal est ubuntu.
- Nous arrivons à connaitre l'existence du fichier data.txt, cependant, il est impossible de connaitre la taille du fichier ainsi que les permissions accordées.
## Question 3

Réponse:
- Tous ses ids sont à 1001 là où pour ubuntu, ils étaient à 1000.
Le processus n'arrive pas à ouvrir mydata.txt
-L'EUID du processus est à 1000 dans ce cas, cependant, il est impossible d'ouvrir mydir.mydata.txt

## Question 4

Réponse

## Question 5

Réponse

## Question 6

Réponse

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








